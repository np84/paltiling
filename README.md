Run "make" to build PALTiling.
To invoke the SpaceInvaders or Alice examples, call in a bash console at the directory of paltiling

```
#!bash
./PALTiling --D myData/spaceInv.jpg.csv --K 10000 --rInc 1 --maxR 9
r=$?
n=384
m=$(wc -l < myData/spaceInv.jpg.csv)
./MatrixViewer myData/spaceInv.jpg.csvpimp.K10000.X $r $n 0 > PimpSpaceInv.X
./MatrixViewer myData/spaceInv.jpg.csvpimp.K10000.Y $m $r 0 > PimpSpaceInv.Y
```
The first line runs PALTiling with the objective of pimp, you can specify the following parameters:

* --rInc: the rank increment (default: 5)
* --K: the number of iterations (default: 1000)
* --D: the path to the data matrix
* --X: path to factor matrix X (default: none - X is initialized randomly)
* --Y: path to factor matrix Y (default: none - X is initialized randomly)
* --A: name of the algorithm, must be "pimp" or "panpal" (default: pimp)
* --maxR: maximum rank to consider (default: 100)
* --dev: number of gpu device (default: 0)
* --t: the threshold increment at which matrices are rounded to binary values in the end (default: 0.05)
* --sep: the separator used in the table of D (default: '\t') 

The last two bash commands convert matrices from its binaries (located at the data file ordner) to TAB-separated csv files.

The Julia script 'TilingGenerator' can be invoked online without installation requirements at [JuliaBox](https://www.juliabox.org/ "juliabox").

We wish you happy pimping!
