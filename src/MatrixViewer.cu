#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <list>

#include <AlgoBase>

int main(int argc, char** argv){

        std::cout << "%MatrixViewer 01APR2016, (c) 2016 Sibylle Hess, Nico Piatkowski, all rights reserved." << std::endl;

	if(argc<4 || argc>5){
		std::cout << "usage: " << argv[0] << " <file> <m> <n> [<prec>]" << std::endl;
		return 500;
	}

	std::string filename(argv[1]);
	size_t m = get<size_t>(argv[2]);
	size_t n = get<size_t>(argv[3]);
	size_t p = 4;

	if(argc==5) p = get<size_t>(argv[4]);

	float* M;
	Matrix::alloc(M, m, n);

	Matrix::read(filename, M, m, n);
	std::cout << std::fixed << std::setprecision(p);
	Matrix::print(M, m, n);

	delete[] M;

	return 0;
}
