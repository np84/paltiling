#include <iostream>
#include <sstream>
#include <iomanip>

#include <Image>

template <class T>
T get(const std::string& s){
	std::stringstream iss(s);
	T val;
	iss >> val;
	return val;
}

int main(int argc, char** args){

	std::cout << "JPEG2CSV 01APR2016, (c) 2016 Sibylle Hess, Nico Piatkowski, all rights reserved." << std::endl;

	if(argc!=2){
		std::cerr << args[0] << " <JPEG-File>" << std::endl;
		return 100;
	}
	std::string jFileName(args[1]);

	size_t bs = 4;

	JPEG<> jpeg(jFileName);
	if(jpeg.width() == 0){
		std::cerr << "ERROR: COULD NOT OPEN " << jFileName << std::endl;
		return 500;
	}
	std::cerr << "JPG.WIDTH  = " << jpeg.width() << std::endl;
	std::cerr << "JPG.HEIGHT = " <<  jpeg.height() << std::endl;

	BITMAP<> bmp(jpeg);

	std::cerr << "WRITING CSV" << std::endl;
	bmp.writeCSVpixBlock(jFileName + ".csv", bs);

	std::cerr << "BITS PER ROW = " << jpeg.width()*jpeg.channels()*8 << std::endl;

	return 0;
}
