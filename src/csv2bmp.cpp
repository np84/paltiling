#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>

#include <Image>

template <class T>
T get(const std::string& s){
	std::stringstream iss(s);
	T val;
	iss >> val;
	return val;
}

int main(int argc, char** args){

	std::cout << "CSV2BMP 01APR2016, (c) 2016 Sibylle Hess, Nico Piatkowski, all rights reserved." << std::endl;

	if(argc!=3){
	std::cerr << args[0] << " <CSV-File> <bits per row>" << std::endl;
	return 100;
	}
	std::string fN(args[1]);

	size_t bitsPerRow	= get<size_t>(args[2]);
	size_t blockSize	= 4;

	std::ifstream csvin(fN);

	if(!csvin.good()){
	std::cerr << "ERROR: COULD NOT OPEN " << fN << std::endl;
	return 500;
	}

	size_t c = 3;
	size_t w = 0;
	size_t h = 0;

	std::vector<bool>* raw = new std::vector<bool>();

	const size_t bits_per_pixel = c*8;
	const size_t blocks_w = ((bitsPerRow/bits_per_pixel)+blockSize-1) / blockSize;
	const size_t b_bits_per_row = blocks_w * blockSize * bits_per_pixel;
	const size_t numWblockBits = blockSize * b_bits_per_row;

	bool* _WblockBits = new bool[numWblockBits];

	size_t blockIdx = 0;
	while(!csvin.eof()){

		std::string line;
		getline(csvin, line);

		if(line.size()>1){
		size_t bidx = 0;
		const size_t blockStart = blockIdx * blockSize * bits_per_pixel;

		std::stringstream lineS(line);
		while(!lineS.eof()){
			std::string val;
			getline(lineS, val, '\t');

			bool bval = false;
			if(val.at(0)=='1') bval = true;

			// bidx == iy * blockSize * bits_per_pixel + ix
			size_t x = bidx % (blockSize * bits_per_pixel);
			size_t y = (bidx - x) / (blockSize * bits_per_pixel);

			const size_t idx = blockStart + y*b_bits_per_row + x;

			//std::cerr << y << ' ' << x << ' ' << raw->size()+idx << ' ' << bval << std::endl;

			_WblockBits[idx] = bval;

			bidx++; // next bit
		}

		blockIdx++; // next block

		if(blockIdx == blocks_w){
			blockIdx = 0;
			for(size_t i=0; i<numWblockBits; ++i)
			raw->push_back(_WblockBits[i]);
		}
		}
	}

	delete[] _WblockBits; 

	csvin.close();
	w = blocks_w*blockSize;
	h = raw->size()/(w*bits_per_pixel);


	// this is unnecessary, but raw->data() does not return pointer to underlying array?!
	bool* rawBits = new bool[raw->size()];
	for(size_t i=0; i<raw->size(); ++i)
	rawBits[i] = raw->at(i);
	delete raw;

	BITMAP<> bmp(rawBits, w, h, c);
	std::cerr << "DIM = " << bmp.width() << " x " << bmp.height() << std::endl;
	std::cerr << "WRITING BMP" << std::endl;
	bmp.write(fN + ".bmp");

	delete[] rawBits;

	return 0;
}
