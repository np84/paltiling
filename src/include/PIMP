#ifndef __PIMP__
#define __PIMP__

#include <AlgoBase>

template <class T>
class SuperPimp: public MF<T>{
	private:
		using MF<T>::n;
		using MF<T>::m;
		using MF<T>::r;
		using MF<T>::D;
		using MF<T>::X;
		using MF<T>::Y;
		using MF<T>::C;
		using MF<T>::nOnes;
		using MF<T>::mOnes;
		using MF<T>::u;
#ifdef __NVCC__
		using MF<T>::handle;
#endif

		/////////////////
		// Helper
		/////////////////
		T* A;
		T* B;
		T* R;
		T* q;
		/////////////////

		/////////////////
		// Constants
		/////////////////
		T* c;
		T* rOnes;
		/////////////////

		const T mu;
		const T gamma;
		const T eps;
		const T lam;

		void stdCompressionSize(T*& _D){
			T *hc;
			Matrix::alloc(hc, this->n, 1);
			T N = 0;
			for(size_t i=0; i<m; ++i){
				for(size_t j=0; j<n; ++j){
					T v = _D[j*m+i];
					hc[j] += v;
					N += v;
				}
			}
			for(size_t j=0; j<n; ++j){
				T v = hc[j] / N; 
				assert(v==v);
				//assert(v>0);
				if(v>0)
				hc[j] = -log(v)/log((T)2);
			}

			cudaMemcpy(c, hc, n*sizeof(T), cudaMemcpyHostToDevice);

			delete[] hc;
		}

	public:
		using MF<T>::getRank;
		using MF<T>::valBinaryPenalizer;

		SuperPimp(T*& _D, const size_t& _m, const size_t& _n, const size_t& _r, const T& _lam = 2, const T& _eps = 1.0) : MF<T>(_D,_m,_n,_r), mu(T(2)*(T(1)+log(n)/log((T)2))), gamma(T(1.000001)), lam(_lam), eps(_eps){

			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);

			(cudaMalloc((void **) &A, r*this->n*sizeof(T)));
			(cudaMalloc((void **) &B, m*r*sizeof(T)));
			(cudaMalloc((void **) &R, r*r*sizeof(T)));

			(cudaMalloc((void **) &q, n*sizeof(T)));
			(cudaMalloc((void **) &c, n*sizeof(T)));

			(cudaMalloc((void **) &rOnes, r*sizeof(T)));
			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);

			stdCompressionSize(_D);

			delete[] hrOnes;
		}

		~SuperPimp(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(R);
			cudaFree(c);
			cudaFree(q);
			cudaFree(rOnes);
		}

		void handleRankInc(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(R);
			cudaFree(rOnes);

			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);

			(cudaMalloc((void **) &A, r*n*sizeof(T)));
			(cudaMalloc((void **) &B, m*r*sizeof(T)));
			(cudaMalloc((void **) &R, r*r*sizeof(T)));

			(cudaMalloc((void **) &rOnes, r*sizeof(T)));
			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);

			delete[] hrOnes;
		}

		T getEps() const {
			return eps;
		}

		T objective_relaxed() const {
			const T a = 1.0, b = 0.0;
			T loss = 0;

			MF<T>::compNoiseRelaxed();
			cublasSnrm2(handle, m*n, C, 1, &loss);	
			loss = mu*0.5*loss*loss;
			
			//regularizer G
			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cudaMemcpy(u, rOnes, r*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSgemv(handle, CUBLAS_OP_T, m, r, &a, Y, m, mOnes, 1, &eps, u, 1);
			thrust::device_ptr<T> uT(u);
			thrust::plus<T> pl;
			const T comp = thrust::transform_reduce(uT, uT+r, entropyfunc(log(Ysum+r*eps)/log(T(2.0))), b, pl);

			T Xcsum = 0;
			cublasSgemv(handle, CUBLAS_OP_N, r, n, &a, X, r, c, 1, &b, u, 1);
			cublasSasum(handle, r, u, 1, &Xcsum);			

			return loss + comp + Xcsum + Ysum+ lam*valBinaryPenalizer();
		}

		T objective() const {
			const T a = 1.0, b = 0.0;

			T Nsum = 0;
			MF<T>::compNoiseBinary();
			cublasSasum(handle, m*n, C, 1, &Nsum);

			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);

			thrust::plus<T> pl;

			cublasSgemv(handle, CUBLAS_OP_T, m, r, &a, Y, m, mOnes, 1, &b, u, 1);
			thrust::device_ptr<T> uT(u);
			const T entY = thrust::transform_reduce(uT, uT+r, entropyfunc(log(Ysum+Nsum)/log((T)2)), T(0), pl);

			thrust::device_ptr<T> CT(C);
			thrust::transform(CT, CT+(m*n), CT, absfunc());
			cublasSgemv(handle, CUBLAS_OP_T, m, n, &a, C, m, mOnes, 1, &b, q, 1);
			thrust::device_ptr<T> qT(q);
			const T entN = thrust::transform_reduce(qT, qT+n, entropyfunc(log(Ysum+Nsum)/log((T)2)), T(0),pl);

			T Xcsum = 0;
			cublasSgemv(handle, CUBLAS_OP_N, r, n, &a, X, r, c, 1, &b, u, 1);
			cublasSasum(handle, r, u, 1, &Xcsum);

			T Ncsum = 0;
			thrust::transform(qT, qT+n, qT, theta<T>(0));
			cublasSdot(handle, n, q, 1, c, 1, &Ncsum);

			return entY + entN + Xcsum + Ncsum;
		}

		// X is r x n
		// Y is m x r
		void iterateX(){
			const T a = 1.0, b = 0.0;

			////////////////////////////////
			// UPDATE X
			////////////////////////////////

			//Compute Gradient in A
			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, n, m, &mu, Y, m, C, m, &b, A, r);
			cublasSger(handle, r, n, &a, rOnes, 1, c, 1, A, r);
			
			//Compute stepsize
			T alpha = 0;
			cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, r, m, &a, Y, m, Y, m, &b, R, r);
			cublasSnrm2(handle, r*r, R, 1, &alpha);
			alpha *= mu*gamma;
			alpha = -1/alpha;

			//Do the step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, r, n, &a, X, r, &alpha, A, r, X, r);

			thrust::device_ptr<T> XT(X);
			thrust::transform(XT, XT+(r*n), XT, proxDelta(-1.0*alpha*lam));
		}

		// X is r x n
		// Y is m x r
		void iterateY(){
			const T a = 1.0, b = 0.0;

			////////////////////////////////
			// UPDATE Y
			////////////////////////////////

			//Compute Gradient in B
			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, m, r, n, &mu, C, m, X, r, &b, B, m); 
			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cudaMemcpy(u, rOnes, r*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSgemv(handle, CUBLAS_OP_T, m, r, &a, Y, m, mOnes, 1, &eps, u, 1);
			thrust::device_ptr<T> uT(u);
			thrust::transform(uT, uT+r, uT, derivYfunc(Ysum+r*eps));
			cublasSger(handle, m, r, &a, mOnes, 1, u, 1, B, m);
			cublasSger(handle, m, r, &a, mOnes, 1, rOnes, 1, B, m);

			//Compute stepsize
			T beta = 0;
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, r, r, n, &a, X, r, X, r, &b, R, r);
			cublasSnrm2(handle, r*r, R, 1, &beta); //beta = ||X^TX||
			beta= mu*beta + 2*m/eps/log(T(2)); //Lipschitz constant of muF + Leps + |Y|
			beta = -1/(gamma*beta);

			//Do the step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, r, &a, Y, m, &beta, B, m, Y, m);

			thrust::device_ptr<T> YT(Y);
			thrust::transform(YT, YT+(r*m), YT, proxDelta(-1.0*beta*lam));
		}

		void iterate(){
			iterateX();
			iterateY();
		}

		void optThreshold(const T& thres, T& _best, T& _x, T& _y, T& _r) {
			cudaMemcpy(A, X, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cudaMemcpy(B, Y, m*r*sizeof(T), cudaMemcpyDeviceToDevice);

			_best = std::numeric_limits<T>::max();
			T rAct =-10;

			for(T x=1.0; x>=0.0; x-=thres){
				for(T y=1.0; y>=0.0; y-=thres){
					this->round(x, y);
					getRank(rAct);
					const T val = objective();

					if(val<=_best){
						_best = val;
						_x = x;
						_y = y;
						_r = rAct;
					}
					cudaMemcpy(X, A, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
					cudaMemcpy(Y, B, m*r*sizeof(T), cudaMemcpyDeviceToDevice);
				}
			}
		}

		struct derivYfunc{
			const T lYspre;

			derivYfunc(T _Yspre) : lYspre(log(_Yspre)/log((T)2)) {}

			__host__ __device__
			T operator()(const T& x) const { 
				return -log(x)/log((T)2) + lYspre;
			}
		};

		struct entropyfunc{
			const T lYspre;

			entropyfunc(T _Yspre) : lYspre(_Yspre) {}

			__host__ __device__
			T operator()(const T& x) const { 
				if(x==T(0)) return 0;
				return -(x+1) * (log(x)/log((T)2)-lYspre);
			}
		};

		struct absfunc{
			__host__ __device__
			T operator()(const T& x) const {
				return abs(x);
			}
		};

		struct proxDelta{
			const T a;

			proxDelta(T _a) : a(_a){}

			__host__ __device__
			T operator()(const T& z) const {
				return (z<=0.5) ? max(T(0), z-a) : min(T(1), z+a);
			}
		};

		struct isOne{
			isOne(){}

			__host__ __device__
			T operator()(const T& z) const {
				return z!=T(1);
			}
		};
};
#endif
