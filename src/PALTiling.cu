#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <list>

#include <PIMP>
#include <PANPAL>
#include <AlgoBase>

int main(int argc, char** argv){

	std::cout << "PALTiling 01APR2016, (c) 2016 Sibylle Hess, Nico Piatkowski, all rights reserved." << std::endl;

	if(argc%2==0){
		std::cout << "usage: " << argv[0] << " --opt1 val1 --opt2 val2 .. --optn valn" << std::endl;
		return 500;
	}

	std::string filename = "";
	std::string Xfilename = "";
	std::string Yfilename = "";

	std::string pimp = "pimp";
	std::string nmfL2 = "panpal";
	std::string algoname = pimp;

	size_t rInc = 5;
	size_t device = 0;
	size_t maxk = 1000;
	size_t maxr = 100;
	float thresinc = 0.05;
	float eps = 1.0;
	float mu = 1.0;
	float lam = 2;
	char sep = '\t';

	for(size_t i=1; i<argc; i+=2){
		std::string opt(argv[i]);
		std::string val(argv[i+1]);

		if(!opt.compare("--rInc")) rInc = get<size_t>(val);
		else if(!opt.compare("--K")) maxk = get<size_t>(val);
		else if(!opt.compare("--D")) filename = val;
		else if(!opt.compare("--X")) Xfilename = val;
		else if(!opt.compare("--Y")) Yfilename = val;
		else if(!opt.compare("--A")) algoname = val;
		else if(!opt.compare("--maxR")) maxr = get<size_t>(val);
		else if(!opt.compare("--eps")) eps = get<float>(val);
		else if(!opt.compare("--mu")) mu = get<float>(val);
		else if(!opt.compare("--lam")) lam = get<float>(val);
		else if(!opt.compare("--dev")) device = get<size_t>(val);
		else if(!opt.compare("--t")) thresinc = get<float>(val);
		else if(!opt.compare("--sep")) sep = get<char>(val);
		else{
			std::cout << "UNKNOWN OPTION: " << opt << std::endl;
			return 700;
		}
	}

	if(!filename.size() ){
		std::cout << "Please specify at least the data matrix, e.g.,\n./PALTiling --D myData/spaceInv.jpg.csv --K 10000 --rInc 1 --maxR 9" << std::endl;
		return 300;
	}

	std::cout << "D\t" << filename << std::endl;
	std::cout << "X\t" << Xfilename << std::endl;
	std::cout << "Y\t" << Yfilename << std::endl;
	std::cout << "ALGO\t" << algoname << std::endl;
	std::cout << "ITERS\t" << maxk << std::endl;
	std::cout << "RANKINC\t" << rInc << std::endl;
	std::cout << "THRESINC\t" << thresinc << std::endl;

	cudaDeviceProp dev;
	cudaGetDeviceProperties(&dev, device);
	cudaSetDevice(device);
	std::cout << "GPU\t" << dev.name << std::endl;

	const char pipe[4] = { '-', '\\', '|', '/' };

	size_t m,n;
	float *D = 0;
	double start = getTime();
	Matrix::read(filename, D, m, n, sep);
	std::cout << "READ DATA TOOK " << getTime()-start << "s" << std::endl;
	MF<float>* algo;

	if(!algoname.compare(pimp)){
		std::cout << "LAM\t" << lam << std::endl;
		std::cout << "EPS\t" << eps << std::endl;
		algo = new SuperPimp<float>(D, m, n, rInc, lam, eps);
	} else if(!algoname.compare(nmfL2)) {
		std::cout << "LAM\t" << lam << std::endl;
		std::cout << "MU\t" << mu << std::endl;
		algo = new NMFL2<float>(D, m, n, rInc, lam, mu);
	} else {
		std::cout << "UNKNOWN ALGORITHM: " << algoname << std::endl;
		std::cout << "CHOOSE EITHER " << pimp << " or " << nmfL2 << std::endl;
		return 700;
	}
	
	if(Xfilename.size() && Yfilename.size() ){
		start = getTime();
		Matrix::readGPU(Xfilename, algo->getX(), rInc, n);
		Matrix::readGPU(Yfilename, algo->getY(), m, rInc);
		std::cout << "READING X AND Y TOOK " << getTime()-start << "s" << std::endl;
	} else {
		std::cout << "INITIALIZE X AND Y RANDOMLY" << std::endl;
	}

	
	  /////////////////////
	 // ALGORITHM BODY  //
	/////////////////////
	float b, x, y, rEst;
	size_t r = rInc;
	int increased =1;
	start = getTime();
	while(increased){
		//Do Optimization 
		for(size_t k=0; k<maxk; ++k){
			algo->iterate();
			//if(showObj) std::cout << i << '\t' << algo->objective() << std::endl;
			std::cout << "\rWORK\t" << pipe[k%4] << " (" << k << ")" << std::flush;
		}
		//Compare rank
		algo->optThreshold(thresinc, b, x, y, rEst);
		std::cout << "rEst:" << rEst << " r:"<< r << std::endl;
		//algo->round(x, y);
		if(rEst<r || r>maxr){
			increased=0;
		}else{
			algo->incrRank(rInc);
			r+=rInc;
		}
	}
	std::cout << "\rWORK\tDONE!" << std::endl;
	algo->round(x, y);
	algo->getRank(rEst);

	std::stringstream dfs;
	dfs <<  filename << algoname << ".K" << maxk;
	std::string fn = dfs.str();

	Matrix::writeGPU(fn+".X", algo->getX(), r, n);
	Matrix::writeGPU(fn+".Y", algo->getY(), m, r);

	std::cout << "ESTIMATED RANK: " << rEst << std::endl;
	std::cout << "WRITE TO: " << fn << std::endl;
	std::cout << "RANK ESTIMATION TOOK " << getTime()-start << "s" << std::endl;

	delete algo;
	delete[] D;

	return r;
}
