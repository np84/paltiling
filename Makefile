NVCC		:= /usr/local/cuda/bin/nvcc
CXX		:= /usr/bin/g++-4.8

OPTS		:= -O3
SRCDIR		:= ./src
OBJDIR		:= ./src
INCLUDES	:= -I./src/include
LIBS		:= -lcublas -ljpeg
CXXFLAGS	:= -std=c++11 $(OPTS) $(INCLUDES) $(LIBS)
LDFLAGS		:= $(OPTS) $(LIBS)
EXEC		:= $(notdir $(basename $(shell ls $(SRCDIR)/*.cu))) $(notdir $(basename $(shell ls $(SRCDIR)/*.cpp)))

.PHONY: clean
.PRECIOUS: $(OBJDIR)/%.o

all: $(EXEC)

PALTiling_bin.tgz:
	@echo "[TAR]\tPALTiling_bin.tgz"
	@tar -czf PALTiling_bin.tgz $(EXEC)

tgz: all PALTiling_bin.tgz

$(OBJDIR)/csv2bmp.o: $(SRCDIR)/csv2bmp.cpp
	@echo "[CXX]\t" $@
	@$(CXX) $(CXXFLAGS) -o $@ -c $(subst .o,.cpp,$(SRCDIR)/$(notdir $@))

$(OBJDIR)/jpg2csv.o: $(SRCDIR)/jpg2csv.cpp
	@echo "[CXX]\t" $@
	@$(CXX) $(CXXFLAGS) -o $@ -c $(subst .o,.cpp,$(SRCDIR)/$(notdir $@))

$(OBJDIR)/%.o: $(SRCDIR)/%.cu
	@echo "[NVCC]\t" $@
	@$(NVCC) -ccbin $(CXX) $(CXXFLAGS) -o $@ -c $(subst .o,.cu,$(SRCDIR)/$(notdir $@))

%: $(OBJDIR)/%.o
	@echo "[LINK]\t" $@
	@$(NVCC) -o $@ $< $(LIBS)

clean:
	@echo "[CLEAN]"
	@rm -rf $(OBJDIR)/*.o $(EXEC)
	@rm -rf $$(find . -iname "*~")
	@rm -ff PALTiling_bin.tgz
